import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NhanVien } from './schemas/nhan-vien.schema';
import { MonAn } from './schemas/mon-an.schema';
import { NguyenLieu } from './schemas/nguyen-lieu.schema';
import { ThucDon } from './schemas/thuc-don.schema';
import { KhachHang } from './schemas/khach-hang.schema';
import { BanAn } from './schemas/ban-an.schema';
import { DatHang } from './schemas/dat-hang.schema';
import { HoaDon } from './schemas/hoa-don.schema';

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://127.0.0.1/quanlynhahang"),
    NhanVien,
    MonAn,
    NguyenLieu,
    ThucDon,
    KhachHang,
    BanAn,
    DatHang,
    HoaDon
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
