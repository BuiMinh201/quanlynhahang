import { Prop,Schema,SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class NguyenLieu {
    @Prop({unique:true,required:true})
    maNguyenLieu:string;
    @Prop({required:true})
    name: string;
    @Prop({required:true})
    source: string;
    @Prop({required:true})
    inventorynumber: string;
}
export const NguyenLieuSchema = SchemaFactory.createForClass(NguyenLieu);