import {Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
@Schema()
export class DatHang {
    @Prop({unique:true,required:true})
    maDH: string;
    @Prop({required:true})
    maKH: string;
    @Prop({required:true})
    maBan: string;
    @Prop({required:false})
    maMonan: string;
    @Prop({required:false})
    soLuong: string;
    @Prop({required:false})
    ngayDH: string;
}
export const DatHangSchema = SchemaFactory.createForClass(DatHang);