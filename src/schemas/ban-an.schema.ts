import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class BanAn {
    @Prop({unique:true,required:true})
    maBan: string;
    @Prop({required:true})
    soGhe: string;
    @Prop({required:true})
    trangThai: string;
}
export const BananSchema = SchemaFactory.createForClass(BanAn);