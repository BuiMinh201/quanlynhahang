import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
@Schema()
export class HoaDon {
    @Prop()
    maHD: string;
    @Prop()
    maDH: string;
    @Prop()
    tongtien: string;
}
export const HoaDonSchema = SchemaFactory.createForClass(HoaDon);