import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class NhanVien {
    @Prop({unique:true,required:true})
    maNV: string;
    @Prop({required:true})
    name: string;
    @Prop({required:true})
    position: string;
    @Prop({required:true})
    address: string;
    @Prop({required:true})
    phone: string;
}
export const NhanVienSchema = SchemaFactory.createForClass(NhanVien)