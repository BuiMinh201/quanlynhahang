import { Prop,Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class MonAn {
    @Prop({unique:true,required:true})
    maMonan: string;
    @Prop({required:false})
    name: string;
    @Prop({required:false})
    loaiMonAn: string;
    @Prop({required:false})
    price: string;
}
export const MonanSchema = SchemaFactory.createForClass(MonAn);
