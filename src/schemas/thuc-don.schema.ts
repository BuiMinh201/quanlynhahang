import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class ThucDon {
    @Prop({unique:true,required:true})
    maThucDon: string;
    @Prop({required:true})
    maMonan: string;
    @Prop({required:true})
    maNguyenLieu: string;
}
export const MenuSchema = SchemaFactory.createForClass(ThucDon);
