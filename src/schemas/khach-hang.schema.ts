import {Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class KhachHang {
    @Prop({unique:true,required:true})
    maKH: string;
    @Prop({required:false})
    tenKH: string;
    @Prop({required:false})
    diaChi: string;
    @Prop({required:false})
    soDienThoai: string;
}
export const KhachhangSchema = SchemaFactory.createForClass(KhachHang);