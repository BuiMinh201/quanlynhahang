import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { NhanVien } from "src/schemas/nhan-vien.schema";
import { createNhanVienDto } from "./dto/nhan-vien.dto";

@Injectable()
export class NhanVienService {
    constructor(
        @InjectModel(NhanVien.name) private NhanVienModel: Model<NhanVien>
    ){}
    createNhanVien(createNhanVienDto: createNhanVienDto) {
        const newNhanVien = new this.NhanVienModel(createNhanVienDto);
        return newNhanVien.save();
    }
}