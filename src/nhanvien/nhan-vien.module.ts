import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { NhanVienSchema } from "src/schemas/nhan-vien.schema";
import { NhanVienService } from "./nhan-vien.service";
import { NhanVienController } from "./nhan-vien.controller";


@Module({
    imports:[
        MongooseModule.forFeature([{
            name: 'NhanVien',
            schema: NhanVienSchema
        },
    ]),
    ],
    providers: [
        NhanVienService
    ],
    controllers:[
        NhanVienController
    ]
})
export class NhanVienModule {}