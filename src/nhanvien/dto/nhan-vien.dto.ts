import { IsNotEmpty, IsString } from "class-validator";

export class createNhanVienDto {
    @IsNotEmpty()
    @IsString()
    maNV: string;
    name: string;
    position: string;
    address: string;
    phone: string;
}