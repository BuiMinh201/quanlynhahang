import { Controller,Post, Body, UsePipes, ValidationPipe } from "@nestjs/common";
import { NhanVienService } from "./nhan-vien.service";
import { createNhanVienDto } from "./dto/nhan-vien.dto";

@Controller('nhanvien')
export class NhanVienController {
    constructor(private nhanVienService: NhanVienService){}
    @Post()
    @UsePipes(new ValidationPipe())
    createNhanVien(@Body() CreateNhanVienDto: createNhanVienDto) {
        console.log(CreateNhanVienDto);
        return this.nhanVienService.createNhanVien(CreateNhanVienDto)
    }
}